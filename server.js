const express = require('express')
const routerStudent = require('./routes/student')

const app = express()

//add the json parser to parse the json data sent through the request body
app.use(express.json())

// use the router to find  all the apis related to the student
app.use(routerStudent)



app.listen(4000, () => {
    console.log('server started on port 4000')
})