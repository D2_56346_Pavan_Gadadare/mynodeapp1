const express = require('express')
const db = require('../db')
const utils = require('../utils')
const mysql2 = require('mysql')

// router which helps user module to add all the routes to the main app
const router = express.Router()


//1.Add Student
router.post('/addStudent', (request, response) => {

    // extract the properties of request.body (json) to
  // const separate variables
  const { roll_no, name, s_class, division,dateofbirth,parent_mobile_no } = request.body

    const connection = db.openConnection()

    //check if the roll_no sent by user already exists in the table
    const roll_no_statement = `select roll_no from student where roll_no = '${roll_no}' `

    connection.query( roll_no_statement , (error,roll_nos) => {
        if(roll_nos.length == 0){
            //no duplicate roll_no
    const statement = `insert into student 
    (roll_no, name, s_class,division,dateofbirth,parent_mobile_no)
    values
    ( '${roll_no}','${name}','${s_class}','${division}','${dateofbirth}','${parent_mobile_no}') 
    `
    connection.query(statement, (error,result) => {
        connection.end()
        response.send({
            status : 'success',
            data : result
        })
    })
        }//end of if
    else{
         //atleast one student exists with this roll_no
         connection.end()
         response.send({
            status : 'error',
            error : 'roll_no already exists'
        })
    }//end of else    
 
}) //end of roll_no_statement   

  })    //end of addStudent

  
//   2. Find Student by rollno

router.get('/findByRoll_no/:id', (request,response) => {

    const {id} = request.params

    const statement = `
                    select roll_no,name, 
                    s_class ,
                    division,
                    dateofbirth,
                    parent_mobile_no 
                    from student
                    where roll_no = ${id}
    `
    const connection = db.openConnection()

    connection.query( statement , (error,records) => {
        connection.end()
        if(records.length>0){
            response.send(records)
        }
        else{
            response.send('student does not exist')
        }
    } )

} )


//   3. Edit specific Student's class and division

router.put('/update/:id', (request,response) => {
    const{id} = request.params

    const{s_class,division} = request.body

    const connection = db.openConnection()
    //check if the roll_no sent by user already exists in the table
    const roll_no_statement = `select roll_no from student where roll_no = '${id}' `

    connection.query( roll_no_statement , (error,roll_nos) => {

        if(roll_nos.length > 0){
            //student exists

        const statement = ` update student
                         set
                        s_class = '${s_class}',
                        division = '${division}'
                        where roll_no = ${id}
                `

               
                connection.query(statement, (error, result) => {
                  connection.end()
                  response.send(utils.createResult(error, result))
                })
            }//end of if
            else{
                    response.send('student does not exists')
            }        

})

})

//   4. Delete Student by id
router.delete('/delete/:id', (request,response) => {


    const {id} = request.params

    const connection = db.openConnection()
    //check if the roll_no sent by user already exists in the table
    const roll_no_statement = `select roll_no from student where roll_no = '${id}' `

    connection.query( roll_no_statement , (error,roll_nos) => {

        if(roll_nos.length > 0){
            //student exists

     const statement = ` delete from student where roll_no = '${id}'  `

    connection.query( statement , (error,records) => {
        connection.end()
        response.send('deleted')
    } )

    }//end of if

    else{
        response.send('student does not exist')
    }

})   

})


//   5. Fetch all students of particular class
router.get('/findByClass/:s_class', (request,response) => {

    const {s_class} = request.params

    const statement = `
                    select *  
                    from student
                    where s_class like '%${s_class}%'
                    `
                    console.log(statement)
    const connection = db.openConnection()

    connection.query(statement, function(error, rows)  {
        
        connection.end()
        response.send(rows)
      })

} )


//   6. Fetch all students of particular birth year

router.get('/year/:birth',(request,response) => {

    const {birth} =request.params
   
    const statement= `
    select * from student where year(dateofbirth)=${birth} `

    console.log(statement)

    const connection = db.openConnection()
    connection.query(statement,(error,student) => {

        connection.end()
        response.send(utils.createResult(error,student))

    })

})




// used to export the router which has all the apis added
module.exports = router